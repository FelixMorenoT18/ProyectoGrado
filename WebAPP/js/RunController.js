(function() {
var app = angular.module('init', []);
var ipServer="10.0.1.35";
var ipServer1="127.0.0.1";
var serverPort="7685";


app.controller("RunController", ["$scope","$http", function($scope, $http){
    $scope.mensaje="Hi";

    $http.get("propertiesKey/MetaDataCassandraAngular.json").then(function (response) {
        $scope.dataC = response.data;

    });

     $scope.captura = function(dato,ncluster) {
        $scope.trama=dato+'~'+ncluster;
         //URL: http://localhost:8080/back?ip=127.0.0.1&frame=k1/pokerhand/3
        $http.get('http://'+ipServer+':'+serverPort+'/back?ip='+ipServer1+'&frame='+$scope.trama).then(success);

       // alert('http://'+ipServer+':'+serverPort+'/back?ip='+ipServer+'&frame='+$scope.trama);
    };

    var success = function(response){

            $scope.r=response;

            $scope.ClusterS=response.data.ClusterSize;
            $scope.ClusterIte=response.data.Iteraciones;
            $scope.DataTamano=response.data.TamanoData;
            $scope.DataTamTrain=response.data.TamanoDataTrain;
            $scope.DataTamTest=response.data.TamanoDataTest;
            $scope.NomKey=response.data.NombeKeySpace;
            $scope.NomTabla=response.data.NombreTabla;
            $scope.NCluster=response.data.NumeroCluster;
            $scope.ErrorCua=response.data.SquaredError;

            console.log("KeySpace " + $scope.NomKey);
            console.log("Tabla " + $scope.NomTabla);
            console.log("Tamano DataSet " + $scope.DataTamano);
            console.log("Tamano DataTrain " + $scope.DataTamTrain);
            console.log("Tamano DataTest " + $scope.DataTamTest);
            console.log("Numero Cluster " + $scope.NCluster);
            console.log("Tamano por Cluster " + $scope.ClusterS);
            console.log("Iteraciones " + $scope.ClusterIte);
            console.log("ErrorCuadratico "  + $scope.ErrorCua);
        }

}]);
}());