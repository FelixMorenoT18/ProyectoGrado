package com.services.main;

import javax.inject.Named;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicesApplicationConfig {

	@Named
	static class JerseyConfig extends ResourceConfig{
		public JerseyConfig() {
			this.packages("com.services.backend");
		}
	}
}
