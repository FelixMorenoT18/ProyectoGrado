package com.services.backend;

import java.util.ArrayList;

import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


import org.json.simple.JSONObject;

import logica.json.JsonGetProperties;
import main.MenuPrincipalMicroServices;


@Named
@Path("/")
public class RestBackEnd {
	
	/*
	 * 
	 * URL: http://localhost:8080/back?ip=127.0.0.1&frame=k1/pokerhand/3
	 * 
	 */

	/*
	 * Trama de Datos [0] Keyspace [1] Table Name [2] Numero Cluster [3] Ruta CSV
	 * [4] Ruta ARFF [5] Tamaño Data [6] Tamaño DataTrain [7] Tamaño DataTest [8]
	 * Resultado ClusterKmeans
	 */

	private MenuPrincipalMicroServices MPB = new MenuPrincipalMicroServices();
	private static JSONObject ResultToFront = new  JSONObject();
	private JsonGetProperties JGP = new JsonGetProperties();
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("back")
	@Produces("application/json")
	public JSONObject RunBack(@QueryParam("ip") String direccion, @QueryParam("frame") String DataFrame) throws Exception {
		
		try {
			ArrayList<Object> AuxDataFrame = new ArrayList<Object>();
			AuxDataFrame = FrameCreator(DataFrame);
			
			
			if (JGP.JsonCheker((String) AuxDataFrame.get(0) + "&" + (String) AuxDataFrame.get(1)) == true) {
				// Existe Registro
				//System.out.println("Aaprobacion del Checker - True");
				ResultToFront = new JSONObject();
				ResultToFront= MPB.CallRunBack(direccion, AuxDataFrame);
				//System.out.println("Antes de salir de getDataFromCassandra " + ResultSetDataMethod.toString());
	
			} else {
				ResultToFront = new JSONObject();
				ResultToFront.put("Error", "000999");
				//System.out.println("Aaprobacion del Checker - False");
			}
			
			
			//System.out.println("Objeto en el MicroServicio " +ResultToFront);
		} catch (Exception e) {
				ResultToFront = new JSONObject();
				ResultToFront.put("Error " + e, "000500");
		}

		return ResultToFront;
	}
	

	private  ArrayList<Object> FrameCreator(String dataFrameFromAngularJS) {
		ArrayList<Object> frame = new ArrayList<Object>();
		try {

			String[] AuxFrame = dataFrameFromAngularJS.split("~");

			for (int i = 0; i < AuxFrame.length; i++) {
				frame.add(AuxFrame[i]);

			}
		} catch (Exception e) {
			System.out.println("ERROR: CassandraConnection - FrameCreator " + e);
		}

		return frame;

	}
	
}
