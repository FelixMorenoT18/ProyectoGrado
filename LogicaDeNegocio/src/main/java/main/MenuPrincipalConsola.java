package main;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import logica.algoritmo.ClusterKm;
import logica.connection.CassandraConnection;
import logica.convert.FileConverter;
import logica.json.JsonCreate;
@SuppressWarnings("unused")

public class MenuPrincipalConsola {

	private static CassandraConnection client = new CassandraConnection();
	private static FileConverter FC = new FileConverter();
	private static ClusterKm cluster = new ClusterKm();
	private static ArrayList<Object> DataFrame;
	
	private static JSONObject ResultadoKmeans;
	private static JsonCreate JsonCreateDataFile = new JsonCreate();

	public static void main(String[] args) throws Exception {
		
		System.out.println("INICIO BACKEND");
		ArrayList<Object> AuxDataFrame= new ArrayList<Object>();
		
		DataFrame = new ArrayList<Object>();
		/*
		 * Trama de Datos 
		 * [0] Keyspace
		 * [1] Table Name
		 * [2] Numero Cluster
		 * [3] Ruta CSV
		 * [4] Ruta ARFF
		 * [5] Tamaño Data
		 * [6] Tamaño DataTrain
		 * [7] Tamaño DataTest
		 * [8] Resultado ClusterKmeans
		 */
		
		/*
		 * Trama JSon
		 * [0] Numero total Cluster (n)
		 * [1 to n] Cantidad datos por cluster
		 * 
		 * */
		
		DataFrame = FrameCreator("k1/pokerhand/10");
		client.conexion("127.0.0.1");
		
		String RCSV = client.getDataFromCassandra(DataFrame);
		DataFrame.add(RCSV);
		
		String RARRF = FC.CallFileLoader(DataFrame);
		DataFrame.add(RARRF);
		
		AuxDataFrame = cluster.clusterKMeans(DataFrame);
		
		DataFrame.add(AuxDataFrame.get(0));
		DataFrame.add(AuxDataFrame.get(1));
		DataFrame.add(AuxDataFrame.get(2));
		DataFrame.add(AuxDataFrame.get(3));
		
		
		//ResultadoKmeans=JsonCreateDataFile.CallCreateJsonFile(DataFrame.get(8));
		
		//System.out.println("JSON MAIN " +ResultadoKmeans.toString());
		
		System.out.println("END BACKEND");

	}

	private static ArrayList<Object> FrameCreator(String dataFrameFromAngularJS) {
		ArrayList<Object> frame = new ArrayList<Object>();
		try {

			String[] AuxFrame = dataFrameFromAngularJS.split("/");

			for (int i = 0; i < AuxFrame.length; i++) {
				frame.add(AuxFrame[i]);

			}
		} catch (Exception e) {
			System.out.println("ERROR: CassandraConnection - FrameCreator " + e);
		}

		return frame;

	}
}
