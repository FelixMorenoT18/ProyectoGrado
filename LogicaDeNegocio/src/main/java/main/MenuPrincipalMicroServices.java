package main;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import logica.algoritmo.ClusterKm;
import logica.connection.CassandraConnection;
import logica.convert.FileConverter;
import logica.hibernate.EntidadDb;
import logica.json.JsonCreate;

@SuppressWarnings("unused")
public class MenuPrincipalMicroServices {
	/**
	 * @author Felix Rafael Moreno Tabares
	 * @version V1.0
	 * @category Machine Learning no Supervisado
	 * @email felixmorenot18@hotmail.com
	 * @Descripcion: Creador de archivos CSV.
	 *
	 */

	/**
	 * Trama de Datos 
	 * [0] Keyspace 
	 * [1] Table Name 
	 * [2] Numero Cluster 
	 * [3] Ruta CSV
	 * [4] Ruta ARFF 
	 * [5] Tamaño Data 
	 * [6] Tamaño DataTrain 
	 * [7] Tamaño DataTest 
	 * [8] Resultado ClusterKmeans
	 */

	/**
	 * 
	 * URL: http://localhost:8080/back?ip=127.0.0.1&frame=k1/pokerhand/3
	 * 
	 */

	private static CassandraConnection client = new CassandraConnection();
	private static FileConverter FC = new FileConverter();
	private static ClusterKm cluster = new ClusterKm();

	private static JSONObject ResultadoKmeans;
	private static JsonCreate JsonCreateDataFile = new JsonCreate();

	public JSONObject CallRunBack(String Ip, ArrayList<Object> CompleteDataFrameFromAngularJS) throws Exception {

		System.out.println("INICIO BACKEND");

		ArrayList<Object> RSult = RunBack(Ip, CompleteDataFrameFromAngularJS);
		ResultadoKmeans = JsonCreateDataFile.CallCreateJsonFile(RSult);

		System.out.println("FIN BACKEND");
		return ResultadoKmeans;
	}

	private ArrayList<Object> RunBack(String Ip, ArrayList<Object> DataFrame) throws Exception {
		ArrayList<Object> AuxDataFrame = new ArrayList<Object>();
		
		client.conexion(Ip);
		String RCSV = client.getDataFromCassandra(DataFrame);
		
		DataFrame.add(RCSV);

		String RARRF = FC.CallFileLoader(DataFrame);
		DataFrame.add(RARRF);

		AuxDataFrame = cluster.clusterKMeans(DataFrame);
	
		for (int i = 0; i < AuxDataFrame.size(); i++) {
			DataFrame.add(AuxDataFrame.get(i));
		}
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		try {
			persist(sessionFactory);
		} finally {
			sessionFactory.close();
		}

		return DataFrame;
	}

	private static void persist(SessionFactory sessionFactory) {
		EntidadDb p1 = new EntidadDb();
		p1.setUser("Felix");
		
		System.out.println("-- persisting persons --");

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p1);
		session.getTransaction().commit();
	}

	private static ArrayList<Object> FrameCreator(String dataFrameFromAngularJS) {
		ArrayList<Object> frame = new ArrayList<Object>();
		try {

			String[] AuxFrame = dataFrameFromAngularJS.split("/");

			for (int i = 0; i < AuxFrame.length; i++) {
				frame.add(AuxFrame[i]);

			}
		} catch (Exception e) {
			System.out.println("ERROR: CassandraConnection - FrameCreator " + e);
		}

		return frame;

	}

}
