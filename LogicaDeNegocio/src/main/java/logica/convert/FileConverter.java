package logica.convert;

import java.io.File;
import java.util.ArrayList;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

/**

 * @author Felix Rafael Moreno Tabares

 * @version V1.0

 * @category Machine Learning no Supervisado

 * @email felixmorenot18@hotmail.com

 * @Descripcion: Clase Transform de datos para la construccion de archivo ARFF.

 *

 */
//String ruta, String nombreTablas, String nC
public class FileConverter {
	private CSVLoader loader = new CSVLoader();
	private ArffSaver saver = new ArffSaver();

	public String CallFileLoader(ArrayList<Object> DataFrame) {

			String FileLoaderResult = FileLoader((String)DataFrame.get(3), (String)DataFrame.get(1));
			
			return FileLoaderResult;
			/*DataFrame.add(FileLoaderResult);
			cluster.clusterKMeans(DataFrame);*/


	}

	private String FileLoader(String CSVPath, String TableName) {

		String nombre = "Resultado.arff";
		String rutaOUT = "../DataConvert/";
		String printOut = rutaOUT + TableName + nombre;

		try {
			loader = new CSVLoader();
			loader.setSource(new File(CSVPath));
			Instances data = loader.getDataSet();

			saver = new ArffSaver();
			saver.setInstances(data);
			saver.setFile(new File(nombre));
			saver.setDestination(new File(rutaOUT + TableName + nombre));
			saver.writeBatch();

			System.out.println("Archivo .arff creado");

		} catch (Exception e) {
			System.out.println("ERROR: FileConverter - FileLoader " + e);
		}
		return printOut;
	}

}
