package logica.make;

import java.io.FileWriter;
import java.util.ArrayList;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import au.com.bytecode.opencsv.CSVWriter;

public class FileBuilder {

	/**
	
	 * @author Felix Rafael Moreno Tabares
	
	 * @version V1.0
	
	 * @category Machine Learning no Supervisado
	
	 * @email felixmorenot18@hotmail.com
	
	 * @Descripcion: Creador de archivos CSV.
	
	 *
	
	 */
	
	
	private static CSVWriter writer;

	public String CallCSVBuilder(String[] headerForCSV, ResultSet results, ArrayList<Object> DataFrame) {

		String CSVBuilderResult = CSVBuilder(headerForCSV, results, (String) DataFrame.get(1));
		//System.out.println("Antes de salir de CSVBuilder " + CSVBuilderResult.toString());
		return CSVBuilderResult; 
		// DataFrame.add(CSVBuilderResult);
		//FC.CallFileLoader(DataFrame);

	}

	private String CSVBuilder(String[] headerDataSet, ResultSet resultadoConsultaExtractor, String tableName) {
		final String add = "Resultado.csv";
		String ruta = "../DataExport/" + tableName + add;

		try {
			writer = new CSVWriter(new FileWriter(ruta));
			writer.writeNext(headerDataSet);

			for (Row r : resultadoConsultaExtractor.all()) {
				// System.out.println("PRUEBA " + r);
				String l1 = r.toString();
				String l2 = l1.substring(4, l1.length() - 1);
				String[] temp = l2.split(",");

				writer.writeNext(temp);

			}

			writer.close();
			System.out.println("Archivo .csv creado exitosamente");

		} catch (Exception e) {
			System.out.println("ERROR: FileBuilder - CSVBuilder " + e);
		}
		return ruta;
	}

}
