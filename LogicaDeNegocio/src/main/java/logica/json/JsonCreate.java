package logica.json;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import logica.algoritmo.Kmeans;


/**

 * @author Felix Rafael Moreno Tabares

 * @version V1.0

 * @category Machine Learning no Supervisado

 * @email felixmorenot18@hotmail.com

 * @Descripcion: Clase almacenar Datos en persistencia.

 *

 */
/*
 * Trama de Datos 
 * [0] Keyspace 
 * [1] Table Name 
 * [2] Numero Cluster 
 * [3] Ruta CSV
 * [4] Ruta ARFF 
 * [5] Tamaño Data 
 * [6] Tamaño DataTrain 
 * [7] Tamaño DataTest 
 * [8] Resultado ClusterKmeans
 * [Size-1] Posiciones Centroides
 */

public class JsonCreate {
	private static Kmeans Modelo = new Kmeans();


	public JSONObject CallCreateJsonFile(ArrayList<Object> Data) {

		JSONObject ObjectResult = CreateJsonFile(Data);
		return ObjectResult;
	}

	@SuppressWarnings("unchecked")
	private JSONObject CreateJsonFile(ArrayList<Object> DataKMeans) {

		JSONObject AuxJsonObject = new JSONObject();

		JSONArray AuxJsonArray = new JSONArray();
		JSONArray AuxJsonArray1 = new JSONArray();

		Modelo = (Kmeans) DataKMeans.get(8);

		System.out.println(Modelo);

		/*
		 * System.out.println("NCluster " + Modelo.getNumClusters());
		 * System.out.println("Iteraciones " + Modelo.getM_Iterations());
		 * System.out.println("Squared error " + Modelo.getSquaredError());
		 * System.out.println("Starting points " +
		 * Modelo.getPuntosIniciales().get(0).toString());
		 */
		
		AuxJsonObject.put("NombeKeySpace", DataKMeans.get(0));
		AuxJsonObject.put("NombreTabla", DataKMeans.get(1));
		AuxJsonObject.put("TamanoData", DataKMeans.get(5));
		AuxJsonObject.put("TamanoDataTrain", DataKMeans.get(6));
		AuxJsonObject.put("TamanoDataTest", DataKMeans.get(7));
		AuxJsonObject.put("NumeroCluster", Modelo.getNumClusters());
		AuxJsonObject.put("Iteraciones", Modelo.getM_Iterations());
		AuxJsonObject.put("SquaredError", Modelo.getSquaredError());
		
		ArrayList<Object> AUX = Modelo.getPuntosIniciales();

		for (int i = 0; i < AUX.size(); i++) {

			AuxJsonArray.add(AUX.get(i).toString());

		}
		AuxJsonObject.put("PuntoInicial", AuxJsonArray);
		
		String auxcor = DataKMeans.get(DataKMeans.size()-1).toString();
		auxcor = auxcor.replaceAll("\\[", "");
		auxcor = auxcor.replaceAll("\\]", "");
		auxcor = auxcor.trim();
		
		String [] auxacorde = auxcor.split(",");
		
		for (int i = 0; i < auxacorde.length; i++) {
			AuxJsonArray1.add(auxacorde[i].trim());
		}
		
		AuxJsonObject.put("CoordenadasCentroides", AuxJsonArray1);
		
		AuxJsonArray = new JSONArray();
		AuxJsonArray.add(Modelo.getClusterSizes());

		AuxJsonObject.put("ClusterSize", AuxJsonArray);
		

		return AuxJsonObject;
	}


	@Autowired
	private void impresoraFrame(ArrayList<Object> frame) {
		for (Object object : frame) {
			System.out.println("Data " + object);
		}
	}
}
