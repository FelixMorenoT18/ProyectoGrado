package logica.json;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonGetProperties {

	/**
	
	 * @author Felix Rafael Moreno Tabares
	
	 * @version V1.0
	
	 * @category Machine Learning no Supervisado
	
	 * @email felixmorenot18@hotmail.com
	
	 * @Descripcion: Control de acceso de tipo de DataSet provenientes del Front.
	
	 *
	
	 */
	
	
	public boolean JsonCheker(String InputFromAngular) {
		boolean checker=false;

		for (int i = 0; i < JsonData().size(); i++) {
			if (InputFromAngular.equals(JsonData().get(i))) {
				checker= true;
			}

		}
		return checker;
	}

	private ArrayList<String> JsonData() {
		String nameKeyspace = "";
		String nameTable = "";
		ArrayList<String> DataJson = new ArrayList<String>();

		try {
			Scanner scanner = new Scanner(new File("../Propiedades/MetaDataCasandra.json"));
			String jsonString = scanner.useDelimiter("\\A").next();
			scanner.close();
			// System.out.println("jsonString =" + jsonString);

			JSONObject empObj = new JSONObject();

			JSONObject jsonObject = new JSONObject(jsonString);

			JSONArray jsonArray = jsonObject.getJSONArray("keyspaces");

			for (int j = 0; j < jsonArray.length(); j++) {
				empObj = jsonArray.getJSONObject(j);

				nameKeyspace = (String) empObj.get("Namekeyspace");
				// System.out.println("Name : " + nameKeyspace);

				nameTable = (String) empObj.get("Nametable");
				// System.out.println("Designation: " + nameTable);
				DataJson.add(nameKeyspace + "&" + nameTable);
			}

		} catch (Exception e) {
			System.out.println("ERROR: JsonGetProperties - JsonCheker " + e);
		}
		return DataJson;
	}
}
