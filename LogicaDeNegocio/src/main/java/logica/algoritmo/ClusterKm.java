package logica.algoritmo;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;



/**

 * @author Felix Rafael Moreno Tabares

 * @version V1.0

 * @category Machine Learning no Supervisado

 * @email felixmorenot18@hotmail.com

 * @Descripcion: Clase encargada de la asignacion de los datos para ejecutar el algoritmo KMeans 

 *

 */
public class ClusterKm {
	private Kmeans kmeansTrain = new Kmeans();
	private Kmeans kmeansTest;
	private Object AuxObjectCoorde;

	
	public ArrayList<Object> clusterKMeans(ArrayList<Object> DataFrame) throws Exception {
		ArrayList<Object> datosR = new ArrayList<Object>();

		try {
			System.out.println("Iniciando Cluster");

			Instances data = (new DataSource((String) DataFrame.get(4))).getDataSet();

			datosR.add(data.size()); // [0] TamaListCoorde Data
			datosR.add(data.trainCV(2, 0).size()); // [1] TamaListCoorde DataTrain
			datosR.add(data.testCV(3, 1).size()); //[2] TamaListCoorde DataTest
			
			

			kmeansTrain.setSeed(10);
			kmeansTrain.setPreserveInstancesOrder(true);
			kmeansTrain.setNumClusters(Integer.parseInt((String) DataFrame.get(2)));
			kmeansTrain.buildClusterer(data.trainCV(2, 0));


			kmeansTest = new Kmeans();
			kmeansTest = kmeansTrain;
			kmeansTest.buildClusterer(data.testCV(3, 1));
			
			datosR.add(kmeansTest);//[3] Resultado Despues de Train

			
	        // print out the cluster centroids
			@SuppressWarnings("unused")
			ArrayList<Object> centroid = new  ArrayList<Object>();
	        Instances centroids = kmeansTest.getClusterCentroids();
	        for (int i = 0; i < centroids.size(); i++) {
	            //System.out.print("Cluster " + i + " size: " + kmeansTest.getClusterSizes()[i]);
	            datosR.add(centroids.instance(i));
	            //System.out.println();
	        }
	      
	        ArrayList<Double> SumCoorde = new ArrayList<Double>();
	        double suma = 0;
	        for (int i = 0; i < Integer.parseInt((String) DataFrame.get(2)); i++) {
	        	AuxObjectCoorde = kmeansTest.getClusterCentroids().get(i);
	        	String auxStringCoorde = (String) AuxObjectCoorde.toString();
				String [] auxStringCoorde1 = auxStringCoorde.split(",");
				
				
				for (int ii = 0; ii < auxStringCoorde1.length; ii++) {
					double valor = 0;
					valor = Double.parseDouble(auxStringCoorde1[ii]);
					suma+=valor;
					
				}
				SumCoorde.add(suma/auxStringCoorde1.length);
				
			}
	        
	        ArrayList<Object> ListCoorde = new ArrayList<Object>();
	        ListCoorde.add(SumCoorde.toString());
	        datosR.add(ListCoorde.toString());
			
		} catch (Exception e) {
			System.out.println("ERROR: clusterKM - clusterKMeans " + e.getLocalizedMessage());
		}

		return datosR;

	}

	@Autowired
	private void NumberFrecuencyCount(Kmeans Modelo) {

		try {
			double[] datosModelo = Modelo.getClusterSizes();

			for (int i = 0; i < datosModelo.length; i++) {
				System.out.println("# Instaces in Cluster [" + (i + 1) + "] - " + datosModelo[i]);
			}
		} catch (Exception e) {
			System.out.println("ERROR: clusterKM - NumberFrecuencyCount " + e);
		}

	}
}
