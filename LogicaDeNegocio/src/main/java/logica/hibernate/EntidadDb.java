package logica.hibernate;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EntidadDb {
	/**
	 * @author Felix Rafael Moreno Tabares
	 * @version V1.0
	 * @category Machine Learning no Supervisado
	 * @email felixmorenot18@hotmail.com
	 * @Descripcion: Clase almacenar Datos en persistencia.
	 *
	 */

    @Id
    @GeneratedValue
    private Integer logID;
    private String user;
    private String data;
    private Integer cluster;
    private Date date;
    
    public EntidadDb() {
    	
    }
    
	public EntidadDb(Integer logID, String user, String data, Integer cluster, Date date) {
		super();
		this.logID = logID;
		this.user = user;
		this.data = data;
		this.cluster = cluster;
		this.date = date;
	}
	
	public Integer getLogID() {
		return logID;
	}
	public void setLogID(Integer logID) {
		this.logID = logID;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Object getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getCluster() {
		return cluster;
	}
	public void setCluster(Integer cluster) {
		this.cluster = cluster;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

    
    
    
}
