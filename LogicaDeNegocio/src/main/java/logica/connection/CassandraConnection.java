package logica.connection;

import java.util.ArrayList;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

import logica.make.FileBuilder;

/**

 * @author Felix Rafael Moreno Tabares

 * @version V1.0

 * @category Machine Learning no Supervisado

 * @email felixmorenot18@hotmail.com

 * @Descripcion: Clase encargada de establecer la conexion con la base de datos y para aplicar un ETL.

 *

 */
public class CassandraConnection {

	private Session session;
	private Cluster cluster;
	private Metadata metadata;
	private HeaderMaker ACM = new HeaderMaker();
	private FileBuilder FB = new FileBuilder();

	public void conexion(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		metadata = cluster.getMetadata();

		// System.out.println("Cassandra connection established");
		// System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

		for (@SuppressWarnings("unused")
		Host host : metadata.getAllHosts()) {
			// System.out.printf("Datatacenter: %s; Host: %s; Rack: %s; \n",
			// host.getDatacenter(), host.getAddress(),host.getRack());
			session = cluster.connect();
			ACM.getConnection(session);

		}
	}

	public String getDataFromCassandra(ArrayList<Object> dataFrameFromAngularJS) {

		// Existe Registro
		// System.out.println("Aaprobacion del Checker - True");
		String ResultSetDataMethod = getAllDataFromCassandra(dataFrameFromAngularJS);
		// System.out.println("Antes de salir de getDataFromCassandra " +
		// ResultSetDataMethod.toString());

		return ResultSetDataMethod;

	}

	private String getAllDataFromCassandra(ArrayList<Object> DFrametData) {

		String[] Head = null;

		String queryConsulting = "SELECT * FROM " + (String) DFrametData.get(0) + "." + (String) DFrametData.get(1)
				+ ";";
		ResultSet results = session.execute(queryConsulting);

		System.out.println("Extraccion exitosa...!");

		Head = ACM.HeaderForCSV((String) DFrametData.get(0), (String) DFrametData.get(1));
		String RutaCSV = FB.CallCSVBuilder(Head, results, DFrametData);
		// System.out.println("Antes de salir de getAllDataFromCassandra " +
		// RutaCSV.toString());
		return RutaCSV;

	}

}
