package logica.connection;

import java.util.ArrayList;

import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
/**
 * @author Felix Rafael Moreno Tabares
 * @version V1.0
 * @category Machine Learning no Supervisado
 * @email felixmorenot18@hotmail.com
 * @Descripcion: Clase Transform de datos para la construccion de archivo CSV.
 *
 */

public class HeaderMaker {

	private Session ConnectionUP;

	public void getConnection(Session session) {
		this.ConnectionUP = session;
	}

	public String[] HeaderForCSV(String nameKeySpace, String tableName) {
		String[] DataHeader = null;

		try {

			KeyspaceMetadata ks = ConnectionUP.getCluster().getMetadata().getKeyspace(nameKeySpace);
			TableMetadata table = ks.getTable(tableName);
			ArrayList<ColumnMetadata> colmds = (ArrayList<ColumnMetadata>) table.getColumns();
			DataHeader = new String[colmds.size()];

			for (int i = 0; i < colmds.size(); i++) {
				DataHeader[i] = colmds.get(i).getName();
			}

		} catch (Exception e) {
			System.out.println("ERROR: AllConnectionMethod - HeaderForCSV: " + e);
		}

		return DataHeader;
	}
}
